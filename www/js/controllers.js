var crntPlatform;
var myMedia;

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $ionicModal, $timeout, $state) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.$on('$ionicView.enter', function(e) {
        var prevButton = jQuery('#prevStep-1');
        var listButton = jQuery('.button');
        var nextButton = jQuery('#nextStep' + $rootScope.steps.length);

        if (!prevButton.hasClass('disabled')) {
            prevButton.addClass('disabled');
        }

        if (!nextButton.hasClass('disabled')) {
            nextButton.addClass('disabled');
        }
        listButton.removeClass('hide');
    });

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Add a new shopping list item to localstorage
    $scope.createTask = function (item) {
        if (item != null) {
            if (/\S/.test(item.title)) {
                $scope.items.push({
                    title: item.title
                });

                window.localStorage['post'] = JSON.stringify($scope.items);

                $scope.modal.hide();
                item.title = "";
            }
        }
    };

    // Load the shopping list items from localstorage
    $scope.items = JSON.parse(window.localStorage['post'] || '[]');

    // Delete an shopping list item from localstorage
    $scope.deleteTask = function (index) {
        $scope.items.splice(index, 1);
        window.localStorage['post'] = JSON.stringify($scope.items);
    };

    // Triggered in the login modal to close it
    $scope.closeNewTask = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.newTask = function() {
        $scope.modal.show();
    };

    $scope.home = function () {
        var button = jQuery('#prevStep-1');
        var listButton = jQuery('.button');
        var nextButton = jQuery('#nextStep' + $rootScope.steps.length);

        if (button.hasClass('disabled')) {
            button.removeClass('disabled');

        }

        if (nextButton.hasClass('disabled')) {
            //nextButton.addClass('enabled');
            nextButton.removeClass('disabled');
        }

        listButton.removeClass('hide');
        $state.go('app.home');
        //$state.reload();

    };

    $scope.homeInfo = function() {
        $state.go('app.home');
    }

    $scope.wassenHome = function () {
        var button = jQuery('#prevStep-1');
        var listButton = jQuery('.button');
        var nextButton = jQuery('#nextStep' + $rootScope.steps.length);

        if (button.hasClass('disabled')) {
            button.removeClass('disabled');
        }

        if (nextButton.hasClass('disabled')) {
            nextButton.removeClass('disabled');
        }
        listButton.removeClass('hide');

        $state.go('app.wassen');
    };

    $scope.playSound = function(sound) {

        sound = 'sound/' + sound;

        if (crntPlatform() == 'android') {
            sound = '/android_asset/www' + sound;
        }

        function onDeviceReady() {
            myMedia = new Media(sound)
        }

        // Roep hier alles aan wat geladen moet worden op 'Device ready'
        document.addEventListener("deviceready", onDeviceReady, false);
        myMedia.play();
    };
})

.controller('listCtrl', function($scope) {
    $scope.data = {
        showDelete: false
    };
})

.controller('HomeCtrl', function($scope, $rootScope) {
    $rootScope.homePages = [
        { id: 0, title: 'Keuken', url: 'keuken', img: 'img/color/afb8.bmp' },
        { id: 1, title: 'Wassen', url: 'wassen', img: 'img/color/afb1.bmp' },
        { id: 2, title: 'Toilet', url: 'toilet', img: 'img/color/afb5.bmp' },
        { id: 3, title: 'Ramen', url: 'ramen', img: 'img/color/afb6.bmp' },
        { id: 4, title: 'Woonkamer', url: 'woonkamer', img: 'img/color/afb4.bmp' },
    ];
})

.controller('WassenCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.wassenPages = [
        { id: 0, title: 'Witte was', url: 'wittewas' },
        { id: 1, title: 'Bonte was', url: 'bontewas' },
        { id: 2, title: 'Spijkerbroeken', url: 'spijkerbroeken' },
        { id: 3, title: 'Bed verschonen', url: 'bedverschonen' },
        { id: 4, title: 'Bed opmaken', url: 'bedopmaken' },
    ];
})

.controller('KeukenCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Dit heb je nodig', content: 'Emmer, Warm water, Werkdoekje, Allesreiniger, Wcreiniger, Toilet borstel, Handschoenen, Dweil', sound: 'keuken/keuken1.mp3'},
        { id: 1, title: 'Stap 1', content: 'Ruim de keuken op (gooi afval weg en doe de afwas) en zorg dat het aanrecht leeg is.', sound: 'keuken/keuken2.mp3'},
        { id: 2, title: 'Stap 2', content: 'Vul een emmer met warm water en doe er een dopje allesreiniger in.', sound: 'keuken/keuken3.mp3'},
        { id: 3, title: 'Stap 3', content: 'Poets het aanrecht, de kastjes en de koelkast aan de buitenkant met een vochtige doekje.', sound: 'keuken/keuken4.mp3'},
        { id: 4, title: 'Stap 4', content: 'Poets de wasbak en kraan met een schuursponsje en schuurmiddel.', sound: 'keuken/keuken5.mp3'},
        { id: 5, title: 'Stap 5', content: 'Spoel het doekje uit en neem de kraan en wasbak af tot er geen schuurmiddel meer op zit.', sound: 'keuken/keuken6.mp3'},
        { id: 6, title: 'Stap 6', content: 'Poets het fornuis. Gebruik indien nodig een klein beetje schuurmiddel en maak het daarna met een vochtig doekje schoon tot er geen schuurmiddel meer op zit.', sound: 'keuken/keuken7.mp3'},
        { id: 7, title: 'Stap 7', content: 'Maak de magnetron schoon met een vochtig doekje. Doe eerst de buitenkant, spoel je doekje schoon en doe daarna de binnenkant.', sound: 'keuken/keuken8.mp3'},
        { id: 8, title: 'Stap 8', content: 'Veeg de vloer schoon met een veger en gooi het vuil in de vuilnisbak.', sound: 'keuken/keuken9.mp3'},
        { id: 9, title: 'Stap 9', content: 'Leeg de vuilnisbak en maak deze met een vochtig doekje schoon voordat je er een nieuwe vuilniszak in doet. Gooi de volle vuilniszak in de container.', sound: 'keuken/keuken10.mp3'},
        { id: 10, title: 'Stap 10', content: 'Maak een emmer met warm water en allesreiniger om de vloer te dweilen. Maak de dweil eerst vochtig en verwijder vlekken op de vloer.', sound: 'keuken/keuken11.mp3'},
        { id: 11, title: 'Stap 11', content: 'Wring de dweil goed uit en ga met een bijna droge dweil nog eens over de vloer. Laat de vloer goed drogen voordat je er overheen loopt.', sound: 'keuken/keuken12.mp3'},
        { id: 12, title: 'Laatste stap', content: 'Doe het doekje en de dweil na het schoonmaken in de was.', sound: 'keuken/keuken13.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.homePages[id].url, id, $rootScope.homePages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('ToiletCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Dit heb je nodig', content: 'Emmer, Warm water, Werkdoekje, Allesreiniger, Wcreiniger, Toilet borstel, Handschoenen, Dweil', sound: 'toilet/toilet1.mp3' },
        { id: 1, title: 'Stap 1', content: 'Spuit wat toiletreiniger in de toiletpot, ga er met de borstel doorheen en laat het even rusten.', sound: 'toilet/toilet2.mp3' },
        { id: 2, title: 'Stap 2', content: 'Vul een emmer met warm water en doe er een dopje allesreiniger in.', sound: 'toilet/toilet3.mp3' },
        { id: 3, title: 'Stap 3', content: 'Poets de buitenkant van het toilet met een vochtig doekje. Spoel het doekje daarna weer uit in de emmer.', sound: 'toilet/toilet4.mp3' },
        { id: 4, title: 'Stap 4', content: 'Poets de wc-bril: eerst de bovenkant, daarna de onderkant. Spoel het doekje goed uit.', sound: 'toilet/toilet5.mp3' },
        { id: 5, title: 'Stap 5', content: 'Poets de rand van het toilet. Spoel het doekje uit onder de kraan en doe hem bij de was.', sound: 'toilet/toilet6.mp3' },
        { id: 6, title: 'Laatste stap', content: 'Gier de emmer met vuil water eerst voorzichtig in het toilet en spoel het toilet door.', sound: 'toilet/toilet7.mp3' }
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.homePages[id].url, id, $rootScope.homePages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('WoonkamerCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Dit heb je nodig', content: 'Emmer, Warm water, Werkdoekje, Stofdoekje, Allesreiniger, Stofzuiger, Dweil', sound:'woonkamer/woonkamer1.mp3'},
        { id: 1, title: 'Stap 1', content: 'Ruim alle losliggende spullen op. Denk aan het opbergen van papierwerk zoals boeken, tijdschriften en je post.', sound:'woonkamer/woonkamer2.mp3'},
        { id: 2, title: 'Stap 2', content: 'Pak de stofdoek en neem hiermee alle meubels (de tv, kasten, planken, tafels en stoelen) af. Tussendoor de stofdoek buiten uitkloppen.', sound:'woonkamer/woonkamer3.mp3'},
        { id: 3, title: 'Stap 3', content: 'Vul een emmer met warm water en doe er een dopje allesreiniger in.', sound:'woonkamer/woonkamer4.mp3'},
        { id: 4, title: 'Stap 4', content: 'Bekijk welke dingen goed tegen een vochtige doek kunnen en neem de tafels, stoele, vensterbank en deuren met een vochtige doek af. Spoel het doekje tussendoor uit in de emmer.', sound:'woonkamer/woonkamer5.mp3'},
        { id: 5, title: 'Stap 5', content: 'Stofzuiger de kamer. Heb je tegels, laminaat of zeil? Dweil hierna dan ook de vloer.', sound:'woonkamer/woonkamer6.mp3'},
        { id: 6, title: 'Stap 6', content: 'Doe het doekje en de dweil na het schoonmaken in de was.', sound:'woonkamer/woonkamer7.mp3'},
        { id: 7, title: 'Laatste stap', content: 'Geef je planten minstens een keer per week water.', sound:'woonkamer/woonkamer8.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.homePages[id].url, id, $rootScope.homePages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('RamenCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Dit heb je nodig', content: 'Emmer, Warm water, Werkdoekje, Stofdoekje, Allesreiniger, Stofzuiger, Dweil', sound: 'ramen/ramen1.mp3'},
        { id: 1, title: 'Stap 1', content: 'Vul een emmer met warm water en doe er een dopje spiritus in.', sound: 'ramen/ramen2.mp3'},
        { id: 2, title: 'Stap 2', content: 'Doe de spons in het water en maak daarmee 1 raam schoon (niet meteen alle ramen).', sound: 'ramen/ramen3.mp3'},
        { id: 3, title: 'Stap 3', content: 'Trek de wisser van boven naar beneden over het raam.', sound: 'ramen/ramen4.mp3'},
        { id: 4, title: 'Stap 4', content: 'Maak de wisser tussendoor droog met een zeem.', sound: 'ramen/ramen5.mp3'},
        { id: 5, title: 'Stap 5', content: 'Maak het raamkozijn droog met een goed uitgewrongen zeem.', sound: 'ramen/ramen6.mp3'},
        { id: 6, title: 'Laatste stap', content: 'Herhaal deze stappen bij de andere ramen.', sound: 'ramen/ramen7.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.homePages[id].url, id, $rootScope.homePages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('WitteWasCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Stap 1', content: 'Sorteer je witte was uit je wasmand (handdoeken, washandjes, sokken, ondergoed etc. als het maar witte/lichte was is).', sound: 'wittewas/wittewas2.mp3'},
        { id: 1, title: 'Stap 2', content: 'Doe de was in de wasmachine.', sound: 'wittewas/wittewas3.mp3'},
        { id: 2, title: 'Stap 3', content: 'Doe het waspoeder (eventueel wasverzachter) in het wasbakje.', sound: 'wittewas/wittewas4.mp3'},
        { id: 3, title: 'Stap 4', content: 'Stel de wasmachine in voor witte was.', sound: 'wittewas/wittewas5.mp3'},
        { id: 4, title: 'Stap 5', content: 'Stel de wasmachine in op 60 graden.', sound: 'wittewas/wittewas6.mp3'},
        { id: 5, title: 'Stap 6', content: 'Sluit de wasmachine en start het programma.', sound: 'wittewas/wittewas7.mp3'},
        { id: 6, title: 'Stap 7', content: 'Als de was klaar is, doe je de deur open, haal je de was er uit en zet je de wasmachine uit.', sound: 'wittewas/wittewas8.mp3'},
        { id: 7, title: 'Laatste stap', content: 'Hang je was op of doe het in de droger.', sound: 'wittewas/wittewas9.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.wassenPages[id].url, id, $rootScope.wassenPages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('BonteWasCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Stap 1', content: 'Sorteer je bonte was uit je wasmand (alle gekleurde was van katoen, zoals truien, t-shirts, sokken etc.).', sound: 'bontewas/bontewas2.mp3'},
        { id: 1, title: 'Stap 2', content: 'Doe de was in de wasmachine', sound: 'bontewas/bontewas3.mp3'},
        { id: 2, title: 'Stap 3', content: 'Doe het waspoeder voor bonte was in het wasbakje.', sound: 'bontewas/bontewas4.mp3'},
        { id: 3, title: 'Stap 4', content: 'Stel de wasmachine in voor bonte was.', sound: 'bontewas/bontewas5.mp3'},
        { id: 4, title: 'Stap 5', content: 'Stel de wasmachine in op 40 graden.', sound: 'bontewas/bontewas6.mp3'},
        { id: 5, title: 'Stap 6', content: 'Sluit de wasmachine en start het programma.', sound: 'bontewas/bontewas7.mp3'},
        { id: 6, title: 'Stap 7', content: 'Als de was klaar is, doe je de deur open, haal je de was er uit en zet je de wasmachine uit.', sound: 'bontewas/bontewas8.mp3'},
        { id: 7, title: 'Laatste stap', content: 'Hang je was op een droogrekje te drogen. Doe g\351\351n kleding in de droger (ze kunnen dan verkleuren of krimpen).', sound: 'bontewas/bontewas9.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.wassenPages[id].url, id, $rootScope.wassenPages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('SpijkerbroekenCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Stap 1', content: 'Sorteer je spijkerbroeken uit je wasmand.', sound:'spijkerbroeken/spijkerbroeken2.mp3'},
        { id: 1, title: 'Stap 2', content: 'Doe je spijkerbroeken (nooit meer dan 5 broeken tegelijk) binnenste buiten in de wasmachine.', sound:'spijkerbroeken/spijkerbroeken3.mp3'},
        { id: 2, title: 'Stap 3', content: 'Doe het waspoeder voor bonte was of waspoeder speciaal voor spijkerbroeken in het wasbakje.', sound:'spijkerbroeken/spijkerbroeken4.mp3'},
        { id: 3, title: 'Stap 4', content: 'Stel de wasmachine in voor de fijne was.', sound:'spijkerbroeken/spijkerbroeken5.mp3'},
        { id: 4, title: 'Stap 5', content: 'Zet de wasmachine op 30 graden.', sound:'spijkerbroeken/spijkerbroeken6.mp3'},
        { id: 5, title: 'Stap 6', content: 'Sluit de wasmachine en start het programma.', sound:'spijkerbroeken/spijkerbroeken7.mp3'},
        { id: 6, title: 'Stap 7', content: 'Als je was klar is, doe je de deur open, haal je de spijkerbroeken er uit en zet je de wasmachine uit.', sound:'spijkerbroeken/spijkerbroeken8.mp3'},
        { id: 7, title: 'Laatste Stap', content: 'Hang je spijkerbroeken op een droogrekje te drogen. Doe g\351\351n spijkerbroeken in de droger (ze kunnen dan verkleuren of krimpen).', sound:'spijkerbroeken/spijkerbroeken9.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.wassenPages[id].url, id, $rootScope.wassenPages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('BedVerschonenCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Stap 1', content: 'Haal het dekbedovertrek van het dekbed en de kussenslopen van je kussens.', sound: 'bedverschonen/bed3.mp3'},
        { id: 1, title: 'Stap 2', content: 'Haal je hoeslaken van je matras.', sound: 'bedverschonen/bed4.mp3'},
        { id: 2, title: 'Stap 3', content: 'Stop je hoeslaken, je dekbedovertrek enj kussenslopen in de wasmachine.', sound: 'bedverschonen/5.mp3'},
        { id: 3, title: 'Stap 4', content: 'Stel de wasmachine in voor witte was (60 graden) en draai de was.', sound: 'bedverschonen/6.mp3'},
        { id: 4, title: 'Stap 5', content: 'Keer je matras om. Zo voorkom je dat je in een kuil ligt en het zorgt dat je matras langer mee gaat!', sound: 'bedverschonen/bed7.mp3'},
        { id: 5, title: 'Stap 6', content: 'Als het kan, hang dan je dekbed ergens overheen zodat het kan luchten.', sound: 'bedverschonen/bed8.mp3'},
        { id: 6, title: 'Laatste stap', content: 'Als het kan, leg je kussens eens in de zon. Zeker als ze plat zijn. Dit komt namelijk door vocht in je kussens. De zon droogt het uit en zorgt dat je kussens weer lekker opgeschud zijn.', sound: 'bedverschonen/bed9.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.wassenPages[id].url, id, $rootScope.wassenPages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('BedOpmakenCtrl', function($scope, $stateParams, $rootScope) {
    $rootScope.steps = [
        { id: 0, title: 'Stap 1', content: 'Doe een schoon hoeslaken over het matras.', sound: 'bedopmaken/bed2.mp3'},
        { id: 1, title: 'Stap 2', content: 'Keer je dekbedovertrek binnenstebuiten.', sound: 'bedopmaken/bed3.mp3'},
        { id: 2, title: 'Stap 3', content: 'Ga met je handen in het dekbedovertrek naar de hoeken toe. Het dekbedovertrek is nu een soort zak om je armen heen.', sound: 'bedopmaken/bed4.mp3'},
        { id: 3, title: 'Stap 4', content: 'Pak met het dekbedovertrek om je armen de hoeken van je dekbed vast.', sound: 'bedopmaken/bed5.mp3'},
        { id: 4, title: 'Stap 5', content: 'Terwijl je de hoeken vast hebt, laat je de rest van het overtrek over het dekbed heen vallen.', sound: 'bedopmaken/bed6.mp3'},
        { id: 5, title: 'Stap 6', content: 'Als het goed is zit het overtrek nu mooi om het dekbed heen, zo niet, schud dan een paar keer.', sound: 'bedopmaken/bed7.mp3'},
        { id: 6, title: 'Stap 7', content: 'Trek de kussenslopen om de kussens.', sound: 'bedopmaken/bed8.mp3'},
        { id: 7, title: 'Laatste stap', content: 'Leg alles op zijn plek, je hebt nu een fris en schoon bed!', sound: 'bedopmaken/bed9.mp3'}
    ]

    var index = $stateParams.state;
    var id = $stateParams.id;

    $scope.page = createContent($rootScope.wassenPages[id].url, id, $rootScope.wassenPages[id].title, $rootScope.steps[index], $rootScope.steps[index].sound);
})

.controller('InfoCtrl', function($scope, $stateParams, $rootScope) {
    // Stuff
});

function createContent(_url, _pageID, _header, step, _sound) {
    var _prev = step.id - 1;
    var _next = step.id + 1;
    var content = [{pageID: _pageID, id: step.id, url: _url, header: _header, title: step.title, content: step.content, sound: _sound, prev: _prev, next: _next}]
    return content;
}

function crntPlatform() {
    return ionic.Platform.platform();
}

function onDeviceReady() {
    myMedia = new Media(sound)
}


document.addEventListener("deviceready", crntPlatform, false);
document.addEventListener("deviceready", onDeviceReady, false);
