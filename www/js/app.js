angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.views.maxCache(0);

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.keuken', {
    url: '/keuken/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/steps.html',
        controller: 'KeukenCtrl'
      }
    }
  })

  .state('app.woonkamer', {
    url: '/woonkamer/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/steps.html',
        controller: 'WoonkamerCtrl'
      }
    }
  })

  .state('app.toilet', {
    url: '/toilet/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/steps.html',
        controller: 'ToiletCtrl'
      }
    }
  })

  .state('app.ramen', {
    url: '/ramen/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/steps.html',
        controller: 'RamenCtrl'
      }
    }
  })

  .state('app.wassen', {
    url: '/wassen/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/wassen.html',
        controller: 'WassenCtrl'
      }
    }
  })

  .state('app.wittewas', {
    url: '/wittewas/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/wassenSteps.html',
        controller: 'WitteWasCtrl'
      }
    }
  })

  .state('app.bontewas', {
    url: '/bontewas/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/wassenSteps.html',
        controller: 'BonteWasCtrl'
      }
    }
  })

  .state('app.spijkerbroeken', {
    url: '/spijkerbroeken/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/wassenSteps.html',
        controller: 'SpijkerbroekenCtrl'
      }
    }
  })

  .state('app.bedverschonen', {
    url: '/bedverschonen/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/wassenSteps.html',
        controller: 'BedVerschonenCtrl'
      }
    }
  })

  .state('app.bedopmaken', {
    url: '/bedopmaken/:id/:state',
    views: {
      'menuContent': {
        templateUrl: 'templates/wassenSteps.html',
        controller: 'BedOpmakenCtrl'
      }
    }
  })

  .state('app.info', {
    url: '/info',
    views: {
      'menuContent': {
        templateUrl: 'templates/info.html',
        controller: 'InfoCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
